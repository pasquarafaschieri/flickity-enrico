<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        //aggiungo questo per l'array css che viene
        //richiamato nel main.php da this->head e va a popolare
        //l'head con tutti i file salvati
        'css/slide.css',
        'https://unpkg.com/flickity@2/dist/flickity.min.css'
    ];
    public $js = [
        'https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js',
        'js/bg-lazyload.js',
        'js/flickity.js'
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
