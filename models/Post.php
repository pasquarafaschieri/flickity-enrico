<?php
//ha creato una classe utilizzabile $p=new Post();
//in realtò stiamo ereditando una serie di funzioni che mi permettodo un getFromDb


namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $abstract
 * @property int $body
 * @property string $date
 * @property string $author
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'abstract', 'body', 'author'], 'required'],
            [['abstract','body'], 'string'],
            [['date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['author'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'abstract' => 'Abstract',
            'body' => 'Body',
            'date' => 'Date',
            'author' => 'Author',
        ];
    }

    /**
     * @inheritdoc
     * @return PostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }
}
